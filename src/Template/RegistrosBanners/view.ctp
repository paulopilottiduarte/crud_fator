<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\RegistrosBanner $registrosBanner
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Editar Banner'), ['action' => 'edit', $registrosBanner->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Excluir Banner'), ['action' => 'delete', $registrosBanner->id], ['confirm' => __('Are you sure you want to delete # {0}?', $registrosBanner->id)]) ?> </li>
        <li><?= $this->Html->link(__('Listar Banners'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Novo Banner'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="registrosBanners view large-9 medium-8 columns content">
    <h3><?= h($registrosBanner->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Nome') ?></th>
            <td><?= h($registrosBanner->nome) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($registrosBanner->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Criado em') ?></th>
            <td><?= h($registrosBanner->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modificado em') ?></th>
            <td><?= h($registrosBanner->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Descrição') ?></h4>
        <?= $this->Text->autoParagraph(h($registrosBanner->descricao)); ?>
    </div>
</div>
