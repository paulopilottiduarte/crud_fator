-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 16-Fev-2018 às 02:49
-- Versão do servidor: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `banner`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `registros_banners`
--

CREATE TABLE `registros_banners` (
  `id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL,
  `nome` varchar(255) NOT NULL,
  `descricao` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `registros_banners`
--

INSERT INTO `registros_banners` (`id`, `created`, `modified`, `nome`, `descricao`) VALUES
(1, '2018-02-15 01:40:55', '2018-02-15 02:54:23', 'Banner padrão.', 'Banner padrão, 12x13. Banner de testes.'),
(3, '2018-02-15 01:56:47', '2018-02-15 01:56:47', 'Banner pequeno.', 'Bannerzinho.'),
(4, '2018-02-15 01:57:02', '2018-02-15 01:57:02', 'Banner grande demais.', 'Bannerzão.'),
(7, '2018-02-15 02:55:57', '2018-02-15 02:55:57', 'Banner padrãozinho', 'Mesmo padrão, só que menor.');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `registros_banners`
--
ALTER TABLE `registros_banners`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `registros_banners`
--
ALTER TABLE `registros_banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
