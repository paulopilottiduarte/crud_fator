<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\RegistrosBanner $registrosBanner
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Form->postLink(
                __('Excluir'),
                ['action' => 'delete', $registrosBanner->id],
                ['confirm' => __('Você está prester a excluir o registro # {0}?', $registrosBanner->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('Listar Banners'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="registrosBanners form large-9 medium-8 columns content">
    <?= $this->Form->create($registrosBanner) ?>
    <fieldset>
        <legend><?= __('Editar Banner') ?></legend>
        <?php
            echo $this->Form->control('nome');
            echo $this->Form->control('descricao');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Enviar')) ?>
    <?= $this->Form->end() ?>
</div>
