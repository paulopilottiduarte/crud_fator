<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RegistrosBannersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RegistrosBannersTable Test Case
 */
class RegistrosBannersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\RegistrosBannersTable
     */
    public $RegistrosBanners;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.registros_banners'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('RegistrosBanners') ? [] : ['className' => RegistrosBannersTable::class];
        $this->RegistrosBanners = TableRegistry::get('RegistrosBanners', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RegistrosBanners);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
