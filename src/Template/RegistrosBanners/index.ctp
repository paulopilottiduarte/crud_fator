<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\RegistrosBanner[]|\Cake\Collection\CollectionInterface $registrosBanners
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Adicionar novo banner'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Filtrar banners'), ['action' => 'search']) ?></li>
    </ul>
</nav>
<div class="registrosBanners index large-9 medium-8 columns content">
    <h3><?= __('Listagem de Banners') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Criado em') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Modificado em') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Nome') ?></th>
                <th scope="col" class="actions"><?= __('Ações') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($registrosBanners as $registrosBanner): ?>
            <tr>
                <td><?= $this->Number->format($registrosBanner->id) ?></td>
                <td><?= h($registrosBanner->created) ?></td>
                <td><?= h($registrosBanner->modified) ?></td>
                <td><?= h($registrosBanner->nome) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Visualizar'), ['action' => 'view', $registrosBanner->id]) ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $registrosBanner->id]) ?>
                    <?= $this->Form->postLink(__('Excluir'), ['action' => 'delete', $registrosBanner->id], ['confirm' => __('Você está prestes a excluir o registro # {0}', $registrosBanner->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('primeiro')) ?>
            <?= $this->Paginator->prev('< ' . __('anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('próximo') . ' >') ?>
            <?= $this->Paginator->last(__('último') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Página {{page}} de {{pages}}, mostrando {{current}} registros de um total de {{count}} registros')]) ?></p>
    </div>
</div>
