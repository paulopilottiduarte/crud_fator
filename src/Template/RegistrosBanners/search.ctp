<?
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\RegistrosBanner $registrosBanner
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Listar Banners'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="registrosBanners form large-9 medium-8 columns content">
    <?= $this->Form->create($registrosBanner) ?>
    <fieldset>
        <legend><?= __('Filtragem de Banners') ?></legend>
        <?php
            echo $this->Form->control('nome');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Filtrar')) ?>
    <?= $this->Form->end() ?>
</div>

?>