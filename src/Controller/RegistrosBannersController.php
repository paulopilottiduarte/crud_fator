<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * RegistrosBanners Controller
 *
 * @property \App\Model\Table\RegistrosBannersTable $RegistrosBanners
 *
 * @method \App\Model\Entity\RegistrosBanner[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class RegistrosBannersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $registrosBanners = $this->paginate($this->RegistrosBanners);
        $this->set(compact('registrosBanners'));
    }

    /**
     * View method
     *
     * @param string|null $id Registros Banner id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $registrosBanner = $this->RegistrosBanners->get($id, [
            'contain' => []
        ]);

        $this->set('registrosBanner', $registrosBanner);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $registrosBanner = $this->RegistrosBanners->newEntity();
        if ($this->request->is('post')) {
            $registrosBanner = $this->RegistrosBanners->patchEntity($registrosBanner, $this->request->getData());
            if ($this->RegistrosBanners->save($registrosBanner)) {
                $this->Flash->success(__('Banner salvo.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Não foi possível salvar o banner.'));
        }
        $this->set(compact('registrosBanner'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Registros Banner id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $registrosBanner = $this->RegistrosBanners->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $registrosBanner = $this->RegistrosBanners->patchEntity($registrosBanner, $this->request->getData());
            if ($this->RegistrosBanners->save($registrosBanner)) {
                $this->Flash->success(__('Banner salvo.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Não foi possível salvar o banner.'));
        }
        $this->set(compact('registrosBanner'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Registros Banner id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $registrosBanner = $this->RegistrosBanners->get($id);
        if ($this->RegistrosBanners->delete($registrosBanner)) {
            $this->Flash->success(__('Banner excluído.'));
        } else {
            $this->Flash->error(__('Não foi possível excluir o banner..'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function search()
    {
    }

}
